# Beamer Theme for Eberhard Karls Universität Tübingen Posters

This is a beamer theme loosely following the Corporate Design of the Eberhard
Karls Universität Tübingen, Germany.

## How to use

See the example file in this repository under `examples/latex/EKUT-poster`.
