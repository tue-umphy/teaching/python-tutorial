#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 28 17:16:47 2023

@author: martin
"""
import pandas as pd
import matplotlib.pyplot as plt

#%%  Load data and have a look at it
df = pd.read_csv("atlantic.csv",comment="#")
print("First lines and columns:")
print(df.head())
print("Dataframe Info:")
print(df.info())
print("Number of dimensions:")
print(df.ndim)

#%% Convert the year/month/day/hours to a unified string
# Take a look at the timestamps we have:
print(df["time"].unique())    

#We need to add trailing "0" to timestamps shorter than 4, to get a uniform timestamp format...
times_string = []
for value in df["time"]:
    if value < 10:
        times_string.append("000" + str(value))
    elif value < 100:
        times_string.append("00" + str(value))
    elif value < 1000: 
        times_string.append("0" + str(value))
    else:
        times_string.append(str(value))
df["time"] = times_string

#Or alternatively just do: df['time'] = df['time'].astype(str).str.rjust(4,"0")

df["day"] = df["day"].astype(str).str.rjust(2,"0")
df["month"] = df["month"].astype(str).str.rjust(2,"0")
df["year"] = df["year"].astype(str)

# Combine the columns into a single string format
df["datetime_str"] = df["year"] + "-" + df["month"] + "-" + df["day"] + " " + df["time"]

#%% Convert to the string to datetime and set as index
# Convert the combined column to datetime with UTC timezone
df['datetime'] = pd.to_datetime(df["datetime_str"], format ="%Y-%m-%d %H%M", utc=True)
df.set_index("datetime", inplace = True)
df["dup"] = df.index.duplicated()
#%% Cool stuff that can be done using the index

# Sort the data using the index
df = df.sort_index()

# Check if the index is unique
df["duplicate_index"] = df.index.duplicated()

# Subset a timespan of the dataset
data_1970 = df['1970-01-01':'1970-12-31']

# Resample 1970 to get daily data
seventies_daily = data_1970.resample('D').first()


#%% Subset data to only the hurricane events

df["status"] = df["status"].str.replace(" ","")

print(df["status"].unique())

only_hurricanes = df[df["status"] == "HU"].copy()

#%% Check in which years hurricanes occured

print(only_hurricanes["year"].unique())

#%% Create a bar plot showing the hurricane events per year:
    
events_per_year = only_hurricanes.groupby(only_hurricanes.index.year)["id"].nunique()
# per decade: only_hurricanes.groupby((only_hurricanes.index.year//10)*10)["id"].nunique()

fig, ax = plt.subplots() 
ax.bar(events_per_year.index,events_per_year)
ax.set_title('Number of Hurricanes per Year')
ax.set_xlabel('Year')
ax.set_ylabel('Number of Hurricanes')

#%% Create a bar plot showing the frequency of hurricanes throughout the year
events_per_month = only_hurricanes.groupby(only_hurricanes.index.month)["id"].nunique()

# Divide the events per month through the total number of events, then multiply by 100 (to get percentages)
events_per_month = events_per_month/sum(events_per_month)*100

# Create a bar plot
fig, ax = plt.subplots() 
ax.bar(events_per_month.index,events_per_month)
ax.set_title('Incidence of Hurricanes in the Atlantic 1851 - 2015')
ax.set_xlabel('Month')
ax.set_ylabel('Incidence of Hurricanes per Month (%)')

#%% Plot the maximum windspeed per latitude/longitude
max_per_lon = only_hurricanes.groupby(only_hurricanes["lon"])["Maximum Wind"].max()
max_per_lat = only_hurricanes.groupby(only_hurricanes["lat"])["Maximum Wind"].max()

fig, ax = plt.subplots() 
ax.scatter(max_per_lon.index,max_per_lon,color = (0.1, 0.2, 0.5, 0.3))
#Here, the color is defined as RGBA value (red,green,blue,alpha) - the last number defines transparency
ax.set_title('Max. Windspeed of Hurricanes in the Atlantic 1851 - 2015')
ax.set_xlabel('Longitude (°)')
ax.set_ylabel('Max. recorded windspeed (knots)')

fig, ax = plt.subplots() 
ax.scatter(max_per_lat.index,max_per_lat,color = (0.1, 0.2, 0.5, 0.3))
#Here, the color is defined as RGBA value (red,green,blue,alpha) - the last number defines transparency
ax.set_title('Max. Windspeed of Hurricanes in the Atlantic 1851 - 2015')
ax.set_xlabel('Latitude (°)')
ax.set_ylabel('Max. recorded windspeed (knots)')

#%% Plot the maximum windspeed per year, and apply a rolling mean

max_per_year = only_hurricanes.groupby(only_hurricanes.index.year)["Maximum Wind"].max()

fig, ax = plt.subplots() 
ax.plot(max_per_year.index,max_per_year,color = (0.1, 0.2, 0.5, 0.3)) 
ax.set_title('Max. Windspeed of Hurricanes in the Atlantic 1851 - 2015')
ax.set_xlabel('Year')
ax.set_ylabel('Max. recorded windspeed (knots)')

#Add a rolling mean of 10 years
max_per_year_rollingmean = max_per_year.rolling(10).median()
ax.plot(max_per_year_rollingmean.index,max_per_year_rollingmean)

#%% Find the frequency of hurricane observations

# Convert index to epochtime in ns, then divide by 10**9 to get seconds
only_hurricanes["epochtime"] = only_hurricanes.index.astype(int) // 10**9
# Get the time difference between two observations
only_hurricanes["timediff"] = only_hurricanes["epochtime"].diff()
median_timediff = only_hurricanes["timediff"].median()
frequency = 1/median_timediff
median_timediff_h = median_timediff/3600

print(f"A hurricane event is tracked with a datapoint every {median_timediff_h} hours, with a frequency of {frequency} Hz.")


#%% Scatterplot of Pressure and Maximal windspeed
# Replace the -999 value in the pressure column with NaN
import numpy as np #we need numpy for the nan
only_hurricanes["Minimum Pressure"] = only_hurricanes["Minimum Pressure"].replace(-999,np.nan)
fig, ax = plt.subplots() 
ax.scatter(only_hurricanes["Minimum Pressure"],only_hurricanes["Maximum Wind"] ,color = (0.1, 0.2, 0.5, 0.3)) 
# Set plot title and labels
ax.set_title('Max. Windspeed and Minimum Pressure of Hurricanes, 1851-2015')
ax.set_xlabel('Pressure (hPa)')
ax.set_ylabel('Max. Windspeed (knots)')

#%% Same scatterplot, but with a regression

hurricanes_cleaned = only_hurricanes.dropna(subset=["Minimum Pressure", "Maximum Wind"]).copy()
hurricane_coefficients = np.polyfit(hurricanes_cleaned["Minimum Pressure"], hurricanes_cleaned["Maximum Wind"], 2)
hurricane_polynomial = np.poly1d(hurricane_coefficients)
print("Polynomial regression function:")
print(hurricane_polynomial)
hurricanes_cleaned["polyfit"] = hurricane_polynomial(hurricanes_cleaned["Minimum Pressure"])
hurricanes_cleaned = hurricanes_cleaned.sort_values(by="Minimum Pressure")


#Plot the fitted and the raw data
fig, ax = plt.subplots() 
ax.scatter(only_hurricanes["Minimum Pressure"],only_hurricanes["Maximum Wind"] ,color = (0.1, 0.2, 0.5, 0.3), label = "raw data") 
ax.plot(hurricanes_cleaned["Minimum Pressure"],hurricanes_cleaned["polyfit"], color = ("red"), label = "polynomial fit")
# Set plot title and labels
ax.set_title('Max. Windspeed and Minimum Pressure of Hurricanes, 1851-2015')
ax.set_xlabel('Pressure (hPa)')
ax.set_ylabel('Max. Windspeed (knots)')
ax.legend()


