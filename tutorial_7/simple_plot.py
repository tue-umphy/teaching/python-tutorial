#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 27 13:40:27 2023

@author: martin
"""
import matplotlib.pyplot as plt

#%% Minimal example for a plot
hours_studied = [2, 4, 6, 8, 10]
test_grade = [4,3,2,1,1]

#%%
fig, ax = plt.subplots()  # Create a figure containing a single axes.
ax.plot(hours_studied,test_grade)  # Plot some data on the axes.

#%% Plot in multiple steps: First, create an empty figure
meinefigure = plt.figure()

#%% Then, add an axes object to the figure
additionalax = meinefigure.add_subplot()

#%% Add data to the axes object and plot
additionalax.plot(hours_studied,test_grade)
plt.show()


#%% Two variants of plotting:
#Object-Oriented:
fig, ax = plt.subplots()
ax.plot(hours_studied,test_grade,label="Test Grades")
ax.set_xlabel("Hours Studied")
ax.set_ylabel("Test Grade")
ax.set_title("Correlation between studying and grades")
ax.legend()

#%% pyplot-style:
plt.figure()
plt.plot(hours_studied,test_grade, label="Test Grades", color = "tab:red")
plt.xlabel("Hours Studied")
plt.ylabel("Test Grade")
plt.title("Correlation between studying and grades")

#%% Convert pandas dataframe columns to numpy arrays with .values first

import pandas as pd

data = {
    "Hours Studied": [2, 4, 6, 8, 10],
    "Test Grade": [4, 3, 2, 1, 1]
}
df = pd.DataFrame(data) #Create an example dataframe

x = df["Hours Studied"].values
y = df["Test Grade"].values
print(type(x))
print(type(y))

fig, ax = plt.subplots()
ax.plot(x,y,label="Test Grades")
ax.set_xlabel("Hours Studied")
ax.set_ylabel("Test Grade")
ax.set_title("Correlation between studying and grades")
ax.legend()

#%% Save the Figure to a png filelist
fig.savefig('grades.png', transparent=False, dpi=80, bbox_inches="tight")
