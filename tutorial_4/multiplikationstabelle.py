# -*- coding: utf-8 -*-
"""
Created on Wed May 24 13:42:40 2023

@author: martin
"""

meine_zahl = float(input("Bitte Zahl eingeben: "))
multiplikatoren = range(1,11)

for zahl in multiplikatoren:
    ergebnis = meine_zahl*zahl
    print(meine_zahl,"x",zahl,"=",ergebnis)
