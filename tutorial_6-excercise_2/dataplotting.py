# -*- coding: utf-8 -*-
"""
Created on Thu Jun 22 12:43:30 2023

@author: schoe
"""

#%% Import packages

import pandas as pd
import matplotlib.pyplot as plt

#%% read data
df1 = pd.read_csv("data1.txt",sep='\t')
df2 = pd.read_csv("data2.csv",comment="#")
df3 = pd.read_csv("data3.dat",sep="\t")

#%% Convert timestamp string to datetime
df1["timesync_timestamp_s"] = pd.to_datetime(df1["timesync_timestamp_s"])
df2["timesync_timestamp_s"] = pd.to_datetime(df2["timesync_timestamp_s"])
df3["timesync_timestamp_s"] = pd.to_datetime(df3["timesync_timestamp_s"])

#%% Set timestamp as index

df1.set_index("timesync_timestamp_s", inplace = True)
df2.set_index("timesync_timestamp_s", inplace = True)
df3.set_index("timesync_timestamp_s", inplace = True)

#%% Concatenate dataframes
df_merge = pd.concat([df1,df2,df3],join="inner")
df_merge.to_csv("data_merge.csv")

#%% Initialize figure and set titles

fig, ax = plt.subplots(figsize=(10,6))
plt.title("Temperature Sensor Readings")
plt.xlabel("Time")
plt.ylabel("Temperature (K)")

#%% Plot BME temperature

ax.plot(df_merge.index, df_merge["bme_temp_k"],label="BME Sensor")

#%% Plot SHT temperature
ax.plot(df_merge.index, df_merge["sht_temp_k"],label="SHT Sensor")

#%% Show legend
ax.legend()

#%% Draw plot
plt.show()

#%% Plotting the scatterplot: Initialize the plot

# Plotting parameters
plt.figure(figsize=(8, 6))
plt.title("Temperature Comparison")
plt.xlabel("SHT Temperature (K)")
plt.ylabel("BME Temperature (K)")

#%% Creating the scatter plot
plt.scatter(df_merge['sht_temp_k'], df_merge['bme_temp_k'])

#%% Displaying the plot
plt.show()



#%% Plot humidity and temperature: Initialize plot

# Plotting parameters
fig, ax1 = plt.subplots(figsize=(10, 6))
plt.title("SHT Temperature and Humidity Sensor Readings")
plt.xlabel("Time")

#%% Plotting SHT Temperature 
ax1.set_ylabel("Temperature (K)", color='tab:red') #set label for y-axis
ax1.plot(df_merge.index, df_merge['sht_temp_k'], color='tab:red') #plot temp to ax1
ax1.tick_params(axis='y', labelcolor='tab:red') #set color for ax1

#%% Plotting humidity on second y-axis

ax2 = ax1.twinx() #make a SECOND y-axis (ax2) with the same x-axis as ax1
mycolor = 'tab:blue'
ax2.set_ylabel("Relative Humidity (%)", color=mycolor)
ax2.plot(df_merge.index, df_merge['sht_relative_humidity_%'], color=mycolor)
ax2.tick_params(axis='y', labelcolor=mycolor)

#%% Add legends
ax1.legend(['Temperature'], loc='upper left')
ax2.legend(['Relative Humidity'], loc='upper right')

#%% Displaying the plot
plt.show()



