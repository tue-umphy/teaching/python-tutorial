import pandas as pd
import matplotlib.pyplot as plt

# Read the CSV file
df = pd.read_csv('temperatur.csv')

# Convert temperature from Kelvin to Celsius
df['Temperatur (C)'] = round(df['Temperatur (K)'] - 273.15,2)

# Convert "Zeit" column to datetime format
df['Zeit'] = pd.to_datetime(df['Zeit'])

df.set_index('Zeit', inplace=True)
# Plot time vs temperature timeseries
plt.plot(df['Temperatur (C)'])
plt.xlabel('Time')
plt.ylabel('Temperature (C)')
plt.title('Temperature vs Time')
plt.xticks(rotation=45)
plt.show()



# Export the converted data to a new CSV file
df.to_csv('converted_temperatures.csv')