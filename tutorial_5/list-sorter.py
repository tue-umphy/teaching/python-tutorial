#Create the list
meineliste = [12.5, "apple", 36, 3.14, "banana", 42, "cat", 56.78, "dog"]


# Create empty lists to store sorted data
float_list = []
string_list = []
integer_list = []

# Iterate through the column data
for value in meineliste:
    if type(value) == float:
        float_list.append(value)
    if type(value) == str:
        string_list.append(value)
    if type(value) == int:
        integer_list.append(value)


# Print the sorted lists
print("Float List:", float_list)
print("String List:", string_list)
print("Integer List:", integer_list)
 
