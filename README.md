# python-tutorial

## For students:
Welcome! 
- This course is available on Ilias or on gitlab: https://gitlab.com/tue-umphy/teaching/python-tutorial - In gitlab, you can add and contribute to this course.
- If you are on gitlab: You can download the complete repository, including all code/exercises and the presentation with the `Download` :arrow_down: button (between the `Find File` and `Clone` buttons).
- The presentation `UWP_1_Python.pdf` file guides you through the tutorial, with the code and data for each exercise in the respective folder. 
- Tutorial 3 and 6 are exercise-only, with tasks that help you deepen your understanding about a topic. The instructions for these exercises are not in the main presentation, but in the folder for the excercise.
- Ilias is really annoying and will add the suffix ".sec" to all ".py" files (or to files without a file ending). Nothing I can do against this, so please rename the files back to their original filename without ".sec" - or just use gitlab to download.

## For new tutors:
 on the server, head to: /Umphy/tutorial/umweltphysik_1/2023_ss and read the readme for more information!

<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://gitlab.com/tue-umphy/teaching/python-tutorial">UWP 1 Python Tutorial</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://gitlab.com/martinschoen">Martin Schön</a> is licensed under <a href="http://creativecommons.org/licenses/by/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"></a></p>
