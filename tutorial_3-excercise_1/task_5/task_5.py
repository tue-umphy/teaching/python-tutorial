import os
import datetime

# Öffne die Textdatei task_5_data.txt und lies den Epoch-Zeitstempel aus der ersten Zeile
with open('task_5_data.txt', 'r') as f:
    epoch_time = int(f.readline().strip())

# Konvertiere den Epoch-Zeitstempel in einen lesbaren String im Format "Jahr-Monat-Tag_Stunde-Minute-Sekunde"
readable_time = datetime.datetime.fromtimestamp(epoch_time).strftime('%Y-%d_%H-%M-%S')

new_file_name = readable_time + ".txt"
# Benenne die Datei task_5_data.txt um in den lesbaren Zeitstempel
os.rename(new_file_name, 'task_5_data.txt')
print(f"Datei wurde in {readable_time}.txt umbenannt.")