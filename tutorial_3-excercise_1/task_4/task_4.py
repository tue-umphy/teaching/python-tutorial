#task 4
import os

files = os.listdir('.')

# Liste alle Dateien im aktuellen Ordner auf
files = os.listdir('.')

files_string = " ".join(files)
print("Folgende Dateien befinden sich in diesem Ordner:")
print(files_string)


# Lasse den Nutzer eine Datei auswählen
selected_file = int(input("Bitte gib den Index der zu umbenennenden Datei an: "))


# Frage nach einem neuen Namen für die Datei
new_name = input("Bitte gib den neuen Namen für die Datei ein: ")

# Benenne die ausgewählte Datei um
os.rename(files[selected_file], new_name)
print("Datei " + files[selected_file] + " wurde in " + new_name  + " umbenannt.")