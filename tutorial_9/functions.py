#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 13 13:11:29 2023

@author: martin
"""
def color_creator(color_word, opacity): 

    if color_word == "red":
        rgba_tuple = (1,0,0,opacity)
    elif color_word == "blue":
        rgba_tuple = (0,0,1,opacity)
    elif color_word == "green":
        rgba_tuple = (0,1,0,opacity)
    else:
        print("Input color not recognized! Setting color to black")
        rgba_tuple = (1,1,1,opacity)

    # And finally, we return the tuple:
    return(rgba_tuple)