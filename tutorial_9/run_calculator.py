#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 13 13:01:40 2023

@author: martin
"""
from calculator import Calculator

# Create an instance of the Calculator class
my_calculator = Calculator()

# Call the add method
result = my_calculator.add(3, 4)
print(result)  # Output: 7

# Call the multiply method
result = my_calculator.multiply(2, 5)
print(result)  # Output: 10